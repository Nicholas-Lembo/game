
//string stuff
struct characterStack{
  char stack[20];
  int ptr;
};



int stringToInt(char num){
  return (int) (num - '0');
}



void pushCharacterStack(struct characterStack *currentStack, char digit){
  currentStack->ptr++;
  currentStack->stack[currentStack->ptr] = digit;
}

char popCharacterStack(struct characterStack *currentStack){
  if(currentStack->ptr > -1){
    char get = currentStack->stack[currentStack->ptr];
    currentStack->ptr--;
    return get;
  }
  return '\0';
}

void readLine(FILE *fptr, char* buffer){
  int i = 0;
  char c = fgetc(fptr);
  while(c != '\n' && c != EOF){
    buffer[i] = c;
    i++;
    //fpos_t pos;
    //fgetpos(fptr, &pos); 
    c = fgetc(fptr);
  } 
  if(c == EOF){
    fseek (fptr,-1, SEEK_CUR );
    //fsetpos(fptr, &pos);
  }
  buffer[i] = '\0';
}

int fileReady(FILE *fptr){
  fpos_t pos;
  fgetpos(fptr, &pos); 
  char c = fgetc(fptr);
  fsetpos(fptr, &pos);
  if(c == EOF) return 1;
  return 0;
}

int whereChar(char* line, char look, int start){
  for(int i = start; line[i] != '\0'; i++){
    if(line[i] == look) return i;
  }
  return -1;
}

void substring(char* dest, char* source, int start, int end){
  int j = 0;
  for(int i = start; i <= end; i++){
    dest[j] = source[i];
    j++;
  }
  dest[j] = '\0';
}


int contains(char* line, char* contains){
  char *p1, *p2, *p3;
  int i=0,j=0;

  p1 = line;
  p2 = contains;

  for(i = 0; i<((int)strlen(line)); i++){
    if(*p1 == *p2){
      p3 = p1;
      for(j = 0;j<((int)strlen(contains));j++){
        if(*p3 == *p2){
          p3++;
          p2++;
        } 
        else
          break;
      }
      p2 = contains;
      if(j == ((int)strlen(contains))){
         return i;
      }
    }
    p1++; 
  }
  return -1;
}

int getNumLines(char**linelist){
  int i = 0;
  while(linelist[i]){
    i++;
  }
  return i;
}

int getMaxLen(char**linelist){
  int maxLen = 0;
  int i = 0;
  while(linelist[i]){
    int len = strlen(linelist[i]);
    if(len > maxLen) maxLen = len;
    i++; 
  }
  return maxLen;
}

