
textbox_Dialogue_link *textbox_initTest(){

  textbox_Dialogue_link* start = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  start->type = TEXT;
  start->member = (textbox_Dialogue_text*)malloc(sizeof(textbox_Dialogue_text));
  ((textbox_Dialogue_text*)start->member)->textline[0] = (char*)malloc(strlen("hello")+1);
  strcpy(((textbox_Dialogue_text*)start->member)->textline[0], "hello");
  ((textbox_Dialogue_text*)start->member)->textline[1] = (char*)malloc(strlen("what is up")+1);
  strcpy(((textbox_Dialogue_text*)start->member)->textline[1], "what is up");
  ((textbox_Dialogue_text*)start->member)->textline[2] = (char*)malloc(strlen("noice")+1);
  strcpy(((textbox_Dialogue_text*)start->member)->textline[2], "noice");
  ((textbox_Dialogue_text*)start->member)->textline[3] = NULL;

  
  textbox_Dialogue_link* two = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  two->type = SELECTION;
  two->member = (textbox_Dialogue_selection*)malloc(sizeof(textbox_Dialogue_selection));
  ((textbox_Dialogue_selection*)two->member)->identifier = (char*)malloc(strlen("gender")+1);
  strcpy(((textbox_Dialogue_selection*)two->member)->identifier, "gender");
  ((textbox_Dialogue_selection*)two->member)->selections[0] = (char*)malloc(strlen("MALE")+1);
  strcpy(((textbox_Dialogue_selection*)two->member)->selections[0], "MALE");
  ((textbox_Dialogue_selection*)two->member)->selections[1] = (char*)malloc(strlen("FEMALE")+1);
  strcpy(((textbox_Dialogue_selection*)two->member)->selections[1], "FEMALE");
  ((textbox_Dialogue_selection*)two->member)->selections[2] = (char*)malloc(strlen("OTHER")+1);
  strcpy(((textbox_Dialogue_selection*)two->member)->selections[2], "OTHER");
  ((textbox_Dialogue_selection*)two->member)->selections[3] = NULL;
  ((textbox_Dialogue_selection*)two->member)->isData = 1;
  ((textbox_Dialogue_selection*)two->member)->isPath = 0;




  textbox_Dialogue_link* three = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  three->type = TYPESCREEN;
  three->member = (textbox_Dialogue_typescreen*)malloc(sizeof(textbox_Dialogue_typescreen));
  ((textbox_Dialogue_typescreen*)three->member)->identifier = (char*)malloc(strlen("name")+1);
  strcpy(((textbox_Dialogue_typescreen*)three->member)->identifier, "name");
  ((textbox_Dialogue_typescreen*)three->member)->display = (char*)malloc(strlen("NAME")+1);
  strcpy(((textbox_Dialogue_typescreen*)three->member)->display, "NAME");



  
  textbox_Dialogue_link* four = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  four->type = TEXT;
  four->member = (textbox_Dialogue_text*)malloc(sizeof(textbox_Dialogue_text));
  ((textbox_Dialogue_text*)four->member)->textline[0] = (char*)malloc(strlen("answer the following")+1);
  strcpy(((textbox_Dialogue_text*)four->member)->textline[0], "answer the folllowing");
  ((textbox_Dialogue_text*)four->member)->textline[1] = NULL;





  textbox_Dialogue_link* five = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  five->type = SELECTION; 
  five->member = (textbox_Dialogue_selection*)malloc(sizeof(textbox_Dialogue_selection));
  ((textbox_Dialogue_selection*)five->member)->identifier = (char*)malloc(strlen("yesno")+1);
  strcpy(((textbox_Dialogue_selection*)five->member)->identifier, "yesno");
  ((textbox_Dialogue_selection*)five->member)->selections[0] = (char*)malloc(strlen("YES")+1);
  strcpy(((textbox_Dialogue_selection*)five->member)->selections[0], "YES");
  ((textbox_Dialogue_selection*)five->member)->selections[1] = (char*)malloc(strlen("NO")+1);
  strcpy(((textbox_Dialogue_selection*)five->member)->selections[1], "NO");
  ((textbox_Dialogue_selection*)five->member)->selections[2] = NULL;
  ((textbox_Dialogue_selection*)five->member)->isPath = 1;
  ((textbox_Dialogue_selection*)five->member)->isData = 0;

  



  textbox_Dialogue_link* six = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  six->type = TEXT;
  six->member = (textbox_Dialogue_text*)malloc(sizeof(textbox_Dialogue_text));
  ((textbox_Dialogue_text*)six->member)->textline[0] = (char*)malloc(strlen("hello")+1);
  strcpy(((textbox_Dialogue_text*)six->member)->textline[0], "hello");
  ((textbox_Dialogue_text*)six->member)->textline[1] = NULL;

  

  
  textbox_Dialogue_link* seven = (textbox_Dialogue_link*)malloc(sizeof(textbox_Dialogue_link));
  seven->type = TEXT;
  seven->member = (textbox_Dialogue_text*)malloc(sizeof(textbox_Dialogue_text));
  ((textbox_Dialogue_text*)seven->member)->textline[0] = (char*)malloc(strlen("hi")+1);
  strcpy(((textbox_Dialogue_text*)seven->member)->textline[0], "hi");
  ((textbox_Dialogue_text*)seven->member)->textline[1] = NULL;




  start->next[0] = two;
  two->next[0] = three;
  three->next[0] = four;
  four->next[0] = five;
  five->next[0] = six;
  five->next[1] = seven;
  six->next[0] = NULL;
  seven->next[0] = NULL;
  return start;
}



void textbox_textProcedure(G_Window *window, state_Message *messagestate, textbox_Dialogue_text *member){
  G_drawImage(window->globalAssets.messageBox, window);
  if(messagestate->startText){
    if (messagestate->halfie || !(member->textline[messagestate->textlineindex])){
      messagestate->startText = 1;
      messagestate->goNextItem = 1;
      messagestate->textlineindex = 0;
      messagestate->enter = 1;
      messagestate->halfie = 0;
      G_freeText(messagestate->textobj[0]); 
      G_freeText(messagestate->textobj[1]); 
      return;
    } 
    else if(member->textline[messagestate->textlineindex] && member->textline[messagestate->textlineindex + 1]){
      if(!messagestate->enter){
        G_freeText(messagestate->textobj[0]); 
        G_freeText(messagestate->textobj[1]); 
      }
      messagestate->textobj[0] = G_createText(member->textline[messagestate->textlineindex], window->globalAssets.font, window->globalAssets.color, window);
      messagestate->textobj[1] = G_createText(member->textline[messagestate->textlineindex + 1], window->globalAssets.font, window->globalAssets.color, window);
    }
    else if(member->textline[messagestate->textlineindex] && !(member->textline[messagestate->textlineindex + 1])){
      if(!messagestate->enter){
        G_freeText(messagestate->textobj[0]); 
        G_freeText(messagestate->textobj[1]); 
      }
      messagestate->textobj[0] = G_createText(member->textline[messagestate->textlineindex], window->globalAssets.font, window->globalAssets.color, window);
      messagestate->textobj[1] = G_createText("", window->globalAssets.font, window->globalAssets.color, window);
      messagestate->halfie = 1;
    }
    messagestate->startText = 0;
  }
  if(messagestate->enter) messagestate->enter = 0;
  if(window->lastKey == A){
    messagestate->startText = 1;
    messagestate->textlineindex += 2;
  }
  messagestate->textobj[0]->x = 32 * 4;
  messagestate->textobj[0]->y = window->height - (32 * 3);
  messagestate->textobj[1]->x = 32 * 4;
  messagestate->textobj[1]->y = window->height - (32 * 2);
  G_drawText(messagestate->textobj[0], window); 
  G_drawText(messagestate->textobj[1], window); 

}




void textbox_drawSelectionBox(int options, int maxLen,  G_Window *window){
  //buffer all sides 3
  G_TileSet *tileset = window->globalAssets.selectionBoxTileSet;
  int rowend;
  for(int i = 0; i < tileset->numTiles; i++){
    tileset->tiles[i]->stretchwidth = 32;
    tileset->tiles[i]->stretchheight = 32;
  }
  if (options > 12) rowend = 15; else rowend = options + 3;
  for(int row = 3; row < rowend + 2; row++){
    for(int col = 3; col < maxLen + 5; col++){
      G_Sprite *sprite;

      if(row == 3 && col == 3)                             sprite = tileset->tiles[0];
      else if(row == 3  && col == (maxLen + 4))            sprite = tileset->tiles[2];
      else if(row == (rowend + 1) && col == 3)             sprite = tileset->tiles[6];
      else if(row == (rowend + 1) && col == (maxLen + 4))  sprite = tileset->tiles[8];
      else if(row == 3)                                    sprite = tileset->tiles[1];
      else if(row == (rowend + 1))                         sprite = tileset->tiles[7];
      else if(col == 3)                                    sprite = tileset->tiles[3];
      else if(col == (maxLen + 4))                         sprite = tileset->tiles[5];
      else                                                 sprite = tileset->tiles[4];

      sprite->x = col*32;
      sprite->y = row*32;
      G_drawSprite(sprite, window);
    }
  }
}

void textbox_drawSelectionText(int numOptions, state_Message *messagestate, G_Window *window){
  int lines;
  if(numOptions > 24 ) lines = 24; else lines = numOptions;
  for(int i = messagestate->top; i < lines; i++){
    messagestate->textobj[i]->y = ((i - messagestate->top) * 32) + (4 * 32);
    messagestate->textobj[i]->x = 5 * 32;
    G_drawText(messagestate->textobj[i], window);
  } 
}


void textbox_selectionProcedure(G_Window *window, state_Message *messagestate, textbox_Dialogue_selection *member){
  if(messagestate->enter){
    window->globalAssets.selectionSprite->stretchheight = 32;
    window->globalAssets.selectionSprite->stretchwidth = 32;
    messagestate->numOptions = getNumLines(member->selections);
    messagestate->maxLen = getMaxLen(member->selections);
    for(int i = 0; i < messagestate->numOptions; i++)
      messagestate->textobj[i] = G_createText(member->selections[i], window->globalAssets.fontsmall, window->globalAssets.color, window);
    if(member->isData){
      messagestate->answerToAdd = (textbox_Answer*) malloc(sizeof(textbox_Answer));
      messagestate->answerToAdd->identifier = (char*)malloc(sizeof (char) * (strlen(member->identifier) + 1));
      strcpy(messagestate->answerToAdd->identifier, member->identifier);
    }
  }
  textbox_drawSelectionBox(messagestate->numOptions, messagestate->maxLen, window);
  textbox_drawSelectionText(messagestate->numOptions, messagestate, window);
  if(window->lastKey == UP && messagestate->optionSelected > 0){
    messagestate->optionSelected--;
    if(messagestate->optionSelected < messagestate->top) messagestate->top++;
  }
  else if(window->lastKey == DOWN && messagestate->optionSelected < (messagestate->numOptions - 1)){
    messagestate->optionSelected++;
    if(messagestate->optionSelected > messagestate->top + 24) messagestate->top--;
  }
  else if(window->lastKey == A){
    if(member->isData){
      messagestate->answerToAdd->type = SELECTION;
      messagestate->answerToAdd->data = (void*) malloc(sizeof(int));
      *((int *)(messagestate->answerToAdd->data)) = messagestate->optionSelected;
      linkedlist_add(messagestate->answer, messagestate->answerToAdd);
    }
    if(member->isPath) messagestate->nextIndex = messagestate->optionSelected;
    messagestate->goNextItem = 1;
    for(int i = 0; i < messagestate->numOptions; i++)
      G_freeText(messagestate->textobj[i]);
    return;     
  }
  window->globalAssets.selectionSprite->x = (32 * 3) + 16;
  window->globalAssets.selectionSprite->y = (32 * 4) + ((messagestate->optionSelected - messagestate->top) * 32);
  G_drawSprite(window->globalAssets.selectionSprite, window);
  if(messagestate->enter) messagestate->enter = 0;
}




void textbox_drawTypescreenText(G_Window *window, state_Message *messagestate){
  int i = 0;
  for(int y = 2; y <= 11 && i < 27; y+=2){
    for(int x = 0; x <= 21 && i < 27; x+=2){
      messagestate->textobj[i]->y = (y * 32) + (4 * 32);
      messagestate->textobj[i]->x = (x * 32) + (4 * 32);
      G_drawText(messagestate->textobj[i], window);
      i++;
    }
  }
  //draw display
  messagestate->textobj[27]->x = 4 * 32;
  messagestate->textobj[27]->y = 4 * 32; 
  G_drawText(messagestate->textobj[27], window);
  G_drawText(messagestate->textobj[28], window);
}


void textbox_createtypescreentext(G_Window *window, state_Message *messagestate, textbox_Dialogue_typescreen *member){
  for(int i = 65; i<91; i++){
    char in[2];
    in[1] = '\0';
    in[0] = i;
    messagestate->textobj[i - 65] = G_createText(in, window->globalAssets.fontsmall, window->globalAssets.color, window);
  }
  messagestate->textobj[26] = G_createText("Enter", window->globalAssets.fontsmall, window->globalAssets.color, window);
  messagestate->textobj[27] = G_createText(member->display, window->globalAssets.fontsmall, window->globalAssets.color, window);
  messagestate->textobj[28] = G_createText(messagestate->name, window->globalAssets.fontsmall, window->globalAssets.color, window);
}


void textbox_drawTypescreenSelector(int optionSelected, G_Window *window){
  window->globalAssets.selectionSprite->x = ((optionSelected % 11) * 64) + (32 * 4);
  window->globalAssets.selectionSprite->y = ((optionSelected / 11) * 64) + (32 * 6);
  G_drawSprite(window->globalAssets.selectionSprite, window);
}



void textbox_typescreenProcedure(G_Window *window, state_Message *messagestate, textbox_Dialogue_typescreen *member){
  if(messagestate->enter){
    textbox_createtypescreentext(window, messagestate, member);
    window->globalAssets.selectionSprite->stretchheight = 32;
    window->globalAssets.selectionSprite->stretchwidth = 32;
  } 
  textbox_drawSelectionBox(13, 22, window);
  if(window->lastKey == LEFT && messagestate->optionSelected > 0) messagestate->optionSelected--;
  if(window->lastKey == RIGHT && messagestate->optionSelected < 26) messagestate->optionSelected++;
  if(window->lastKey == UP && (messagestate->optionSelected / 11) > 0) messagestate->optionSelected-=11;
  if(window->lastKey == DOWN && (messagestate->optionSelected / 11 < 1 || (messagestate->optionSelected / 11 == 1 && messagestate->optionSelected % 11 < 5))) messagestate->optionSelected+=11;
  printf("%d\n" , messagestate->optionSelected);
  if(window->lastKey == B && strlen(messagestate->name) > 0){
    int len = strlen(messagestate->name);
    messagestate->name[len - 1] = '\0';
    G_freeText(messagestate->textobj[28]);
    messagestate->textobj[28] = G_createText(messagestate->name, window->globalAssets.fontsmall, window->globalAssets.color, window);
    messagestate->textobj[28]->x = (5 * 32) + messagestate->textobj[27]->width;
    messagestate->textobj[28]->y = 4 * 32;  
  }
  if(window->lastKey == A){
    if(messagestate->optionSelected == 26){
      char *nameAnswer = (char*)malloc(sizeof(char*) * (strlen(messagestate->name) + 1));
      strcpy(nameAnswer, messagestate->name);
      messagestate->answerToAdd->data = nameAnswer;
      linkedlist_add(messagestate->answer, messagestate->answerToAdd);
      for(int i = 0; i <= 28; i++) G_freeText(messagestate->textobj[i]);
      messagestate->goNextItem = 1;
      return;
    }
    if(strlen(messagestate->name) < 18){
      G_freeText(messagestate->textobj[28]);
      char tmp[2];
      tmp[0] = 65 + messagestate->optionSelected;
      tmp[1] = '\0';
      strcat(messagestate->name, tmp);
      messagestate->textobj[28] = G_createText(messagestate->name, window->globalAssets.fontsmall, window->globalAssets.color, window);
      messagestate->textobj[28]->x = (5 * 32) + messagestate->textobj[27]->width;
      messagestate->textobj[28]->y = 4 * 32;
    }  
  }
  textbox_drawTypescreenText(window, messagestate);
  textbox_drawTypescreenSelector(messagestate->optionSelected, window);
  if(messagestate->enter) messagestate->enter = 0;
}



int textbox_run(G_Window *window, state_State *state, state_Message *messagestate, textbox_Dialogue_link **current){
  if(*current){
    switch((*current)->type){
      case TEXT:
        textbox_textProcedure(window, messagestate, (*current)->member);
        break;
      case SELECTION:
        textbox_selectionProcedure(window, messagestate, (*current)->member);
        break;
      case TYPESCREEN:
        textbox_typescreenProcedure(window, messagestate, (*current)->member);
        break;
    }
    if(messagestate->goNextItem){
      messagestate->current = (textbox_Dialogue_link*)((*current)->next[messagestate->nextIndex]); 
      messagestate->optionSelected = 0;
      messagestate->nextIndex = 0;
      messagestate->top = 0;
      messagestate->startText = 1;
      messagestate->enter = 1;
      messagestate->goNextItem = 0;
      messagestate->numOptions = 0;
      if(!(messagestate->current)) return 1;
    }
    return 0;
  } 
}











