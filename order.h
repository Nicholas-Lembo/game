//constants


typedef enum {NONE, QUIT, UP, DOWN, LEFT, RIGHT, A, B, SELECT, START} Keypress;





//G_Font
typedef TTF_Font G_Font;


//G_Text
typedef struct{
  SDL_Texture *texture;
  char* text;
  int x;
  int y;
  int width;
  int height;
}G_Text;

typedef SDL_Color G_Color;


//G_Image
typedef struct {
  const char *path;
  SDL_Texture *texture;
  int x;
  int y;
  int width;
  int height;
  int orig_width;
  int orig_height;
} G_Image;

//G_Sprite
typedef struct {
  const char *path;
  G_Image *img;
  int x;
  int y;
  int width;
  int height;
  int stretchwidth;
  int stretchheight;
  int clip_width;
  int clip_height;
  void *nextSprite;
} G_Sprite;

//G_TileSet
typedef struct {
  G_Image *img;
  int xOffset;
  int yOffset;
  int numTiles;
  G_Sprite **tiles;
}G_TileSet;

//G_Map
typedef struct{
  int **layer;
  int numRows;
  int numCols;
  G_TileSet* tileset;
}G_Map;


struct GlobalAssets{
  G_Image *messageBox;
  G_Color color;
  G_Font *font;
  G_Font *fontsmall;
  G_Sprite *selectionSprite;
  G_TileSet *selectionBoxTileSet;
};

//G_Window
typedef struct{
  SDL_Window *sdl;
  SDL_Renderer *renderer;
  Keypress lastKey;
  int width;
  int height;
  int fps;
  struct GlobalAssets globalAssets;
} G_Window;



typedef enum {LOOP, ONCE} AnimationType;
typedef enum {PAUSE, CONTINUE} AnimationState;



typedef enum {OVERWORLD, BATTLE, STARTSCREEN, EXPOSITION, QUITGAME} GameState;
typedef enum {NORMAL, MENU, MENU_CHARACTERS, MENU_ITEMS, DIALOGUE } OverWorldState;
typedef enum {MAP1, MAP2} MapState;


//info_Character
typedef struct{
  const int attack;
  const int defense;
  const int speed;
  const int hp;
  const double levelrate;
}info_Character;


//state_Character
typedef struct{
  int level;
  int exp;
  int attack;
  int defense;
  int speed;
  int hp;
  info_Character *characterclass;
}state_Character;


//state_State
typedef struct{
  GameState gameState;
  MapState currentMap;
  char textboxon;
  state_Character *characters[5];
  int playerx;
  int playery;
}state_State;

//state_TitleScreenState
typedef struct{
  int init;
  G_Image *img[2];
  G_Sprite *sprite;
  G_Text *text[2];
  int selected;
}state_TitleScreenState;



//state_ExpositionState
typedef struct{
}state_ExpositionState;





//state_OverWorldState
typedef struct{
  OverWorldState state;
}state_OverWorldState;



//textbox_Dialogue
typedef enum{TEXT, SELECTION, TYPESCREEN, END}textbox_Order;




//textbox_Dialogue_text
typedef struct{
  char *textline[20];
}textbox_Dialogue_text;


//textbox_Dialogue_typescreen
typedef struct{
  char *display;
  char *identifier;
}textbox_Dialogue_typescreen;

//textbox_Dialogue_selection
typedef struct{
  char *identifier;
  char *selections[10];
  char isPath;
  char isData;
}textbox_Dialogue_selection;


//textbox_Dialogue_link
typedef struct{
  void *next[4];
  textbox_Order type;
  void *member;
}textbox_Dialogue_link;


//textbox_Answer
typedef struct{
  char *identifier;
  textbox_Order type;
  void *data;
}textbox_Answer;



//state_Message
typedef struct{
  G_Text *textobj[40];//holds text objects
  int textlineindex;//holds the current index within a char* array
  char halfie;//if a textbox contains only one line
  char enter; //upon entering a new node (to avoid freeing when not allocated yet
  char startText; //if starting a new box
  char goNextItem; //advance to the next item
  int optionSelected; //whichever option the user has selected
  int nextIndex; //index for "next" text_box_dialogue_link
  int top; //when a selection list is present it is the top most element
  int numOptions; //holds the number of options
  int maxLen; //holds length of the longest word in a list
  char name[20]; //buffer for the typscreen
  textbox_Dialogue_link *start;
  textbox_Dialogue_link *current;
  Linkedlist *answer;
  textbox_Answer *answerToAdd;
}state_Message;


G_Window *G_init(int width, int height, int fps, char* title);
void G_clear(G_Window *window);
void G_update(G_Window *window);
void G_updateEvents(G_Window *window);
void G_quit(G_Window* window);
G_Image *G_loadImage(char* path, G_Window* window);
G_Sprite *G_createSprite(G_Image *img, int width, int height, int clip_width, int clip_height);
void G_freeSprite(G_Sprite *sprite);
void G_drawImage(G_Image *img, G_Window *window);
void G_drawSprite(G_Sprite *sprite, G_Window *window);
G_Sprite* G_createAnimationSprite(G_Sprite* sprites[], AnimationType type);
void G_freeImage(G_Image* img);
G_TileSet *G_createTileSet(G_Image* img, int xOffset, int yOffset); 
G_Map *G_createMap(const char* path, G_TileSet *tileset);
void G_drawMap(G_Map *map, G_Window *window, int startrow, int startcol);
G_Font *G_loadFont(char* path, int pt);
void G_freeFont(G_Font *font);
G_Text *G_createText(char *text, G_Font *font, G_Color color, G_Window *window);
void G_drawText(G_Text *textobj, G_Window *window);
void G_initGlobalAssets(G_Window *window);



state_State *state_initState();
state_Message *state_initMessageState();
void state_freeTitleScreen(state_TitleScreenState **title); 
void state_startScreen(G_Window *window, state_State *state, state_TitleScreenState **title);
void state_overworld(G_Window *window, state_State *state);
void state_battle(G_Window *window, state_State *state);
int textbox_run(G_Window *window, state_State *state, state_Message *messagestate, textbox_Dialogue_link **current);
void state_exposition(G_Window *window, state_State *state, state_Message *messagestate, state_ExpositionState *expositionstate);
int state_checkState(G_Window *window, 
    state_State *state,
    state_Message *messagestate,
    state_OverWorldState *overworldstate,
    state_TitleScreenState **titlestate,
    state_ExpositionState *expositionstate);




textbox_Dialogue_link *textbox_initTest();
