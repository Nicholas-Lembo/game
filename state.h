
state_State *state_initState(){
  state_State* state = (state_State*) malloc(sizeof(state_State));
  state->gameState = STARTSCREEN;
  state->currentMap = MAP1;
  state->playerx = 5;
  state->playery = 5;
  state->textboxon = 0;
  return state;
}

state_Message *state_initMessageState(){
  state_Message* state = (state_Message*) malloc(sizeof(state_Message));
  state->textlineindex = 0;
  state->goNextItem = 0;
  state->startText = 1;
  state->start = NULL;
  state->current = NULL;
  state->enter = 1;
  state->optionSelected = 0;
  state->nextIndex = 0;
  state->top = 0;
  state->halfie = 0;
  state->answer = (Linkedlist*)malloc(sizeof(Linkedlist*));
  state->answer->len = 0;
  state->answer->next = NULL;
  state->name[0] = '\0';
  return state;
}

void state_freeTitleScreen(state_TitleScreenState **titlestate){ 
  G_freeText((*titlestate)->text[0]);
  G_freeText((*titlestate)->text[1]);
  free(*titlestate);
  *titlestate = NULL;
}

void state_startScreen(G_Window *window, state_State *state, state_TitleScreenState **titlestate){
  if(*titlestate == NULL){
    *titlestate = (state_TitleScreenState*) malloc(sizeof(state_TitleScreenState));
    (*titlestate)->selected = 1;
    window->globalAssets.selectionSprite->x = 370;
    window->globalAssets.selectionSprite->stretchheight = 32;
    window->globalAssets.selectionSprite->stretchwidth = 32;
    G_Color color;
    color.r = 0;
    color.g = 0;
    color.b = 0;
    color.a = 255;
    (*titlestate)->text[0] = G_createText("START", window->globalAssets.font, color, window);
    (*titlestate)->text[0]->x = (window->width / 2) - ((*titlestate)->text[0]->width /2);
    (*titlestate)->text[0]->y = 450;
    (*titlestate)->text[1] = G_createText("QUIT", window->globalAssets.font, color, window);
    (*titlestate)->text[1]->x = (window->width / 2) - ((*titlestate)->text[1]->width /2);
    (*titlestate)->text[1]->y = 490;
  }
  G_drawText((*titlestate)->text[0], window);
  G_drawText((*titlestate)->text[1], window);


  if(window->lastKey == UP){
    (*titlestate)->selected = 1;
  }
  if (window->lastKey == DOWN) (*titlestate)->selected = 0;


  if ((*titlestate)->selected){
    window->globalAssets.selectionSprite->y = 450;
  }
  else{
    window->globalAssets.selectionSprite->y = 490;
  }


  
  if(window->lastKey == A){
    if((*titlestate)->selected){
      state_freeTitleScreen(titlestate);
      state->gameState = EXPOSITION;
      return;
    }
    else{
      state_freeTitleScreen(titlestate);
      state->gameState = QUITGAME;
      return;
    }
  }

  G_drawSprite(window->globalAssets.selectionSprite, window);
}


void state_overworld(G_Window *window, state_State *state){
  G_Image *img = G_loadImage("TileSets/Start.bmp", window);
  G_Sprite *sprite = G_createSprite(img, 16, 32, 3 * 16 , 2 * 16);
  sprite->x = window->width/2;
  sprite->y = window->height/2;
  G_TileSet *set = G_createTileSet(img, 16, 16);
  G_Map *map = G_createMap("Maps/start.csv", set);
}

void state_battle(G_Window *window, state_State *state){
  
}

void state_exposition(G_Window *window, state_State *state, state_Message *messagestate, state_ExpositionState *expositionstate){
  if(messagestate->start == NULL){
    messagestate->start = textbox_initTest();
    messagestate->current = messagestate->start;
  }
  textbox_run(window, state, messagestate, &(messagestate->current)); 
}



int state_checkState(G_Window *window,
    state_State *state,
    state_Message *messagestate,
    state_OverWorldState *overworldstate,
    state_TitleScreenState **titlestate,
    state_ExpositionState *expositionstate){


  switch(state->gameState){
    case OVERWORLD:
      state_overworld(window, state);
      break;
    case BATTLE:
      state_battle(window, state);
      break;
    case STARTSCREEN:
      state_startScreen(window, state, titlestate);
      break;
    case EXPOSITION:
      state_exposition(window, state, messagestate, expositionstate);
      break;
    case QUITGAME:
      return 0;
  }
  return 1;
}

