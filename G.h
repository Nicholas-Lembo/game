

G_Window *G_init(int width, int height, int fps, char* title){
  SDL_Window* window = NULL;
  G_Window* gwindow = (G_Window*)malloc(sizeof(G_Window));
  TTF_Init();
  if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0){
    printf("SDL could not be initialized. ERROR: %s\n", SDL_GetError());
  }
  if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    return -1;
  //create a window
  window = SDL_CreateWindow(
    title,
    SDL_WINDOWPOS_UNDEFINED,
    SDL_WINDOWPOS_UNDEFINED,
    width,
    height,
    SDL_WINDOW_SHOWN);
  if(window == NULL){
    printf("window could not be created. ERROR: %s\n", SDL_GetError());
  }
  SDL_Renderer *renderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
  SDL_SetRenderDrawColor( renderer, 0xFF, 0xFF, 0xFF, 0xFF );
  gwindow->width = width;
  gwindow->height = height;
  gwindow->sdl = window;
  gwindow->fps = fps;
  gwindow->renderer = renderer;
  gwindow->lastKey = NONE;
  return gwindow;
}

void G_clear(G_Window *window){
  SDL_RenderClear(window->renderer);
}
void G_update(G_Window *window){
  SDL_RenderPresent(window->renderer); 
}
void G_updateEvents(G_Window *window){
  SDL_Event e;
  window->lastKey = NONE;
  while(SDL_PollEvent(&e) != 0) { 
    if(e.type == SDL_QUIT){
      window->lastKey = QUIT;
    } 
    else if( e.type == SDL_KEYDOWN ) {
      switch( e.key.keysym.sym ){ 
        case SDLK_UP: 
          window->lastKey = UP;
          break; 
        case SDLK_DOWN: 
          window->lastKey = DOWN;
          break; 
        case SDLK_LEFT: 
          window->lastKey = LEFT;
          break; 
        case SDLK_RIGHT: 
          window->lastKey = RIGHT;
          break; 
        case SDLK_z: 
          window->lastKey = A;
          break; 
        case SDLK_x: 
          window->lastKey = B;
          break; 
      }
    } 
  }
}

void G_quit(G_Window* window){
  //Destroy_Window
  SDL_DestroyWindow(window->sdl);
  SDL_DestroyRenderer(window->renderer);
  //quit SDL
  Mix_Quit();
  SDL_Quit();
}

G_Image *G_loadImage(char* path, G_Window* window){
  //allocate memory for structure
  G_Image* img = (G_Image*)malloc(sizeof(G_Image));
  //load surface
  SDL_Surface *surface = NULL;
  surface = SDL_LoadBMP(path);
  if(surface == NULL) printf("image didn't load");
  img->texture = SDL_CreateTextureFromSurface( window->renderer, surface );
  img->path = path;
  img->x = 0;
  img->y = 0;
  img->orig_width = surface->w;
  img->orig_height = surface->h;
  img->width = img->orig_width;
  img->height = img->orig_height; 
  SDL_FreeSurface(surface);
  return img;
}

G_Sprite *G_createSprite(G_Image *img, int width, int height, int clip_width, int clip_height){
  G_Sprite *sprite = (G_Sprite*) malloc(sizeof(G_Sprite));
  sprite->path = img->path;
  sprite->img = img;
  sprite->x = 0;
  sprite->y = 0; 
  sprite->width = width;
  sprite->height = height;
  sprite->stretchwidth = width;
  sprite->stretchheight = height;
  sprite->clip_width = clip_width;
  sprite->clip_height = clip_height;
  return sprite;
}

void G_freeSprite(G_Sprite *sprite){
  free(sprite);
}




void G_drawImage(G_Image *img, G_Window *window){
  SDL_Rect selectRect;
  selectRect.x = img->x;
  selectRect.y = img->y;
  selectRect.w = img->width;
  selectRect.h = img->height;
  SDL_RenderCopy( window->renderer, img->texture, NULL, &selectRect );
}

void G_drawSprite(G_Sprite *sprite, G_Window *window){
  SDL_Rect selectRect;
  selectRect.x = sprite->x;
  selectRect.y = sprite->y;
  selectRect.w = sprite->stretchwidth;
  selectRect.h = sprite->stretchheight;
  SDL_Rect clipRect;
  clipRect.x = sprite->clip_width;
  clipRect.y = sprite->clip_height;
  clipRect.w = sprite->width;
  clipRect.h = sprite->height;
  SDL_RenderCopy( window->renderer, sprite->img->texture, &clipRect, &selectRect );
}


//null terminated list
G_Sprite* G_createAnimationSprite(G_Sprite* sprites[], AnimationType type){
  int index = 0;
  G_Sprite *currentFrame = sprites[index];
  while(currentFrame){
    index++;
    currentFrame->nextSprite = sprites[index];
  }
  if(type == LOOP){
    currentFrame->nextSprite = sprites[0];
  }
  return sprites[0];
}
  

void G_freeImage(G_Image* img){
  SDL_DestroyTexture(img->texture);
  free(img);
}


G_TileSet *G_createTileSet(G_Image* img, int xOffset, int yOffset){ 
  G_TileSet* tileset = (G_TileSet*) malloc(sizeof(G_TileSet));
  int size = 0;
  for(int i = 0; i < img->height; i += xOffset){
    for(int j = 0; j < img->width; j += yOffset){
      size++; 
    }
  } 
  G_Sprite **tiles = (G_Sprite**) malloc(sizeof(G_Sprite*) * size);
  int num = 0;
  for(int i = 0; i < img->height; i += xOffset){
    for(int j = 0; j < img->width; j += yOffset){
      tiles[num] = G_createSprite(img, xOffset, yOffset, j, i);
      num++;
    }
  } 
  tiles[num] = NULL;
  tileset->xOffset = xOffset;
  tileset->yOffset = yOffset;
  tileset->numTiles = size;
  tileset->tiles = tiles;
  tileset->img = img;
  return tileset;
}


G_Map *G_createMap(const char* path, G_TileSet *tileset){
  FILE *fptr = fopen(path, "r");
  int layerplace = 0;
  int row = 0;
  int **layer = (int**)malloc(sizeof(int*) * 1000); 
  G_Map *map = (G_Map*)malloc(sizeof(G_Map));
  struct characterStack currentStack;
  currentStack.ptr = -1;
  char c = fgetc(fptr);
  int *layerrow = (int*)malloc(sizeof(int));
  while(c != EOF){
    if(c >= 48 && c <= 57)
      pushCharacterStack(&currentStack, c);
    else if(c == ','){
      int place = 1;
      int numToInsert = 0;
      char d = popCharacterStack(&currentStack);
      while(d != '\0'){
        int value = stringToInt(d);
        value *= place;
        numToInsert += value;
        place *= 10; 
        d = popCharacterStack(&currentStack);
      }
      layerrow[layerplace] = numToInsert;
      layerplace++;

    }
    else if(c == '\n'){
     map->numCols = layerplace;
     layer[row] = layerrow;
    row++;
     layerrow = (int*)malloc(sizeof(int) * 1000);
     layerplace = 0;
    }
    c = fgetc(fptr);
  }
  free(layerrow);
  fclose(fptr); 
  map->numRows = row;
  map->layer = layer;
  map->tileset = tileset;
  return map;
}


void G_drawMap(G_Map *map, G_Window *window, int startrow, int startcol){
  int xOffset = map->tileset->xOffset;
  int yOffset = map->tileset->yOffset;
  for(int i = 0; (i * yOffset) < window->height; i++){
    for(int j = 0; (j * xOffset) < window->width; j++){
      G_Sprite *sprite = map->tileset->tiles[map->layer[startrow + i][startcol + j]];
      sprite->x = j * xOffset;
      sprite->y = i * yOffset;
      G_drawSprite(sprite, window);
    }
  }
}


G_Font *G_loadFont(char* path, int pt){
  G_Font *font= TTF_OpenFont(path, pt);
  return font;
}

void G_freeFont(G_Font *font){
  TTF_CloseFont(font);
}

G_Text *G_createText(char *text, G_Font *font, G_Color color, G_Window *window){
  G_Text *textobj = (G_Text*)malloc(sizeof(G_Text));
  char *textref = (char*) malloc(strlen(text) + 1);
  strcpy(textref, text);
  SDL_Surface *surface = TTF_RenderText_Solid(font, text, color);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(window->renderer, surface);
  SDL_FreeSurface(surface);
  int w, h;
  SDL_QueryTexture(texture, NULL, NULL, &w, &h);
  textobj->width = w;
  textobj->height = h;
  textobj->texture = texture;
  textobj->text = textref;
  return textobj;
}

void G_freeText(G_Text* textobj){
  free(textobj->text);
  SDL_DestroyTexture(textobj->texture);
  free(textobj);
}

void G_drawText(G_Text *textobj, G_Window *window){
  SDL_Rect selectRect;
  selectRect.x = textobj->x;
  selectRect.y = textobj->y;
  selectRect.w = textobj->width;
  selectRect.h = textobj->height;
  SDL_RenderCopy( window->renderer, textobj->texture, NULL, &selectRect);
}


void G_initGlobalAssets(G_Window *window){
  window->globalAssets.messageBox = G_loadImage("Images/message.bmp", window);
  window->globalAssets.messageBox->width = (window->globalAssets.messageBox->width) * 4;
  window->globalAssets.messageBox->height = (window->globalAssets.messageBox->height) * 4;
  window->globalAssets.messageBox->x = 80;
  window->globalAssets.messageBox->y = window->height - (window->globalAssets.messageBox->height);
  window->globalAssets.font = G_loadFont("Fonts/font.ttf", 32);
  window->globalAssets.fontsmall = G_loadFont("Fonts/font.ttf", 24);
  window->globalAssets.color.r = 0;
  window->globalAssets.color.g = 0;
  window->globalAssets.color.b = 0;
  window->globalAssets.color.a = 255;
  
  
  G_Image *tmp = G_loadImage("TileSets/StartScreen.bmp", window);
  window->globalAssets.selectionSprite = G_createSprite(tmp, 16, 16, 0, 0);
   
  tmp = G_loadImage("TileSets/SelectionBox.bmp", window);
  window->globalAssets.selectionBoxTileSet = G_createTileSet(tmp, 16, 16);
}
