# set the compiler
CC = gcc
#set compiler flags
CFLAGS = -Wall -g -Wextra -fmax-errors=5 -lSDL2 -lSDL2_ttf -lSDL2_mixer
#target executable flags
TARGET = game

all: $(TARGET)

$(TARGET): $(TARGET).c
	$(CC) $(CFLAGS) -o $(TARGET) $(TARGET).c

clean:
	$(RM) $(TARGET)
