//Linkedlist
typedef struct{
  int len;
  void *data;
  void *next;
}Linkedlist;

void  *linkedlist_get(Linkedlist* start, int index){
  for (int i = 0; i <= index; i++){
    start = (Linkedlist*) start->next;
  }
  return start->data;
}

void linkedlist_add(Linkedlist* start, void *data){
  start->len++;
  while (start->next != NULL){
    start = (Linkedlist*)start->next;
  }
  Linkedlist* add = (Linkedlist*) malloc(sizeof(Linkedlist));
  add->data = data;
  add->next = NULL;
  start->next = add;
}


void linkedlist_set(Linkedlist *start, int index, void *data){
  for (int i = 0; i <= index; i++){
    start = (Linkedlist*) start->next;
  }
  start->data = data;
}

void linkedlist_getArray(Linkedlist* start, void  **buffer){ 
  int len = start->len;
  for(int i = 0; i < len; i++){
    buffer[0] = start->data;
    start = start->next;
  }
}

void linkedlist_free(Linkedlist* start){
  int len = start->len;
  start->len = 0; 
  start = start->next;
  Linkedlist *nextNode = start->next;
  for(int i = 0; i < len - 1; i++){
    free(start);
    start = nextNode;
    nextNode = start->next;
  }
}


void linkedlist_insert(Linkedlist* start, int index, void *data){
  start->len++;
  for(int i = 0; i < index; i++){
    start = start->next;
  }
  Linkedlist *next = start->next;
  Linkedlist* add = (Linkedlist*) malloc(sizeof(Linkedlist));
  add->data = data;
  start->next = add;
  add->next = next;
}


/*
void linkedlist_print(Linkedlist* start){ 
  int len = start->len;
  for(int i = 0; i < len; i++){
    printf("index %d: %d\n", i,  ((Linkedlist*) start->next)->data);
    start = start->next;
  }
}
*/
