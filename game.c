//includes
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>  
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <string.h>
#include "linkedlist.h"
#include "helper.h"
#include "order.h"
#include "G.h"
#include "info.h"
#include "state.h"
#include "textbox.h"

int main(int argc, char* args[]){
  G_Window *window = G_init(960, 640, 60, "GAME");   
  G_initGlobalAssets(window);
  state_State *state = state_initState();
  state_Message *messagestate = state_initMessageState();
  state_TitleScreenState *titlestate = NULL;
  state_OverWorldState *overworldstate = NULL;
  state_ExpositionState *expositionstate = NULL;
  int cont = 1;
  Mix_Music *music = Mix_LoadMUS("Music/RUSSIAN.aiff");
  if (!Mix_PlayingMusic()) SDL_ShowSimpleMessageBox(NULL, "", "No music playing", window);
  if (music == NULL) printf("hello");
  if (Mix_PlayMusic( music, -1)  == -1) printf("hello");
  while(cont){
    G_clear(window);
    cont = state_checkState(window, state, messagestate, overworldstate, &titlestate, expositionstate);
    G_updateEvents(window); 
    G_update(window);
    SDL_Delay(30);
  }
  G_quit(window);
  return 0;
}
